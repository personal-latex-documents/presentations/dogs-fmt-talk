# Talk content outline

## Preliminaries

- Abelian varieties
- Coherent sheaves
    - vector bundles
    - skyscraper
- $D^b(X)$
    - skim over given title of seminar

Next 2 sections "elephants in the room": things people ought to now even if they don't care about the later details

## Orlov theorems, how FMT fits in general geometry

## How it's similar to Fourier transforms

Fourier transform | Fourier Mukai transform
-|-
non-obvious equiv. $L^2(V) \to L^2(V^{*})$ | non-obvious equiv. $D^b(X) \to D^b(\hat{X})$
$\sin(\omega x) \mapsto \mathbb{1}_\omega$ | ${P}_{\hat{x}} \mapsto {O}_{\hat{x}}[-dim(X)]$
aoeu | aoeu 

## Definition and basic lemmas (homalg)

- Go over definition a bit more thoroughly (cover derived subtleties)
- composition of FMTs is an FMT

## Cohomological Fourier-Mukai Transform

- Mukai lattice / Chern characters / Chow group
    - skim over links
    - bilinear form on lattice
    - sometimes, we just care about the action of FMTs on lattice
- Action of FMT is a lattice isom

### CFMT for PPAS

- describe action in terms of $SL(2)$
- translating between pairs of semi-homog vector bundles viewpoint


